package tests;

import static org.junit.Assert.*;

import org.junit.*;

import cs200spring2018team3.Service;

public class ServiceTest {

	@Test
	public void testObject() {
		Service service = new Service();
		service.setFee(34.25);
		service.setCode(45);
		service.setDescription("Pet Sitting");
		service.setProviderID(3);
		service.setName("Jacob Fulmer");
		assertEquals(34.25, service.getFee(), 0.01);
		assertEquals(45, service.getCode());
		assertEquals("Pet Sitting", service.getDescription());
		assertEquals(3, service.getProviderID());
		assertEquals("Jacob Fulmer", service.getName());
	}

	@Test
	public void testConstructorFull() {
		Service service = new Service(34.25, 45, "Pet Sitting", 3, "Jacob Fulmer"); 
		assertEquals(34.25, service.getFee(), 0.01);
		assertEquals(45, service.getCode());
		assertEquals("Pet Sitting", service.getDescription());
		assertEquals(3, service.getProviderID());
		assertEquals("Jacob Fulmer", service.getName());
	}
	@Test
	public void testConstructorEmpty() {
		Service service = new Service();

		assertEquals(0.0, service.getFee(), 0.01);
		assertEquals(0, service.getCode());
		assertEquals("", service.getDescription());
		assertEquals(0, service.getProviderID());
		assertEquals("", service.getName());
	}

}
