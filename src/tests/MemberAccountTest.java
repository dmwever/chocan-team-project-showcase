package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import cs200spring2018team3.Address;
import cs200spring2018team3.MemberAccount;

public class MemberAccountTest {

	@Test
	public void testObject() {
		MemberAccount account = new MemberAccount();
		Address address = new Address();
		account.setAccountName("Bob Ross");
		account.setAccountAddress(address);
		account.setAccountEmail("bross@gmail.com");
		account.setAccountNumber(1776);
		account.setSuspension(false);
		assertEquals("Bob Ross", account.getAccountName());
		assertEquals("bross@gmail.com", account.getAccountEmail());
		assertEquals(address, account.getAccountAddress());
		assertEquals(1776, account.getAccountNumber());
		assertEquals(false, account.getSuspension());
	}

	@Test
	public void testConstructorFull() {
		Address address = new Address();
		MemberAccount account = new MemberAccount("Bob Ross", 1776, address, "bross@gmail.com", false);
		assertEquals("Bob Ross", account.getAccountName());
		assertEquals("bross@gmail.com", account.getAccountEmail());
		assertEquals(address, account.getAccountAddress());
		assertEquals(1776, account.getAccountNumber());
		assertEquals(false, account.getSuspension());
	}
	
	@Test
	public void testConstructorEmpty() {
		MemberAccount account = new MemberAccount();
		assertEquals("", account.getAccountName());
		assertEquals("", account.getAccountEmail());
		assertNull(account.getAccountAddress());
		assertEquals(0, account.getAccountNumber());
		assertFalse(account.getSuspension());
	}
	

}
