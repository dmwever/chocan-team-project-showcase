package tests;

/**
 * Description: ServiceRecordControllerTest class. This class test the ServiceRecordController class by creating three ServiceRecord 
 * 				objects, adding them to a list in ServiceRecordController, and testing functions in the ServiceRecordController class.
 * 				The methods tested include the ServiceRecordController constructor, createServiceRecord(), getServiceRecordListSize(), 
 * 				deleteServiceRecord(), and getServiceRecord().
 * @author Daniel Michaels
 * 
 */

import static org.junit.Assert.*;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import cs200spring2018team3.MemberAccount;
import cs200spring2018team3.ProviderAccount;
import cs200spring2018team3.Service;
import cs200spring2018team3.ServiceRecord;
import cs200spring2018team3.ServiceRecordController;

public class ServiceRecordControllerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSuccess1() {
		
		//Create first service to be added to the ServiceRecordController
		Service service1 = new Service(12,123456,"First Service",654321,"Aaron");
		LocalDateTime serviceTime1=null;
		ProviderAccount provider1 = new ProviderAccount();
		MemberAccount member1 = new MemberAccount();
		String comments1 = "This is the first service.";
		LocalDateTime currentTime1=null;
		
		ServiceRecord serviceRecord1 = new ServiceRecord(serviceTime1,service1,provider1,member1,comments1,currentTime1);
		
		//Create second service to be added to the ServiceRecordController
		Service service2 = new Service(17,246802,"Second Service",101010,"Zack");
		LocalDateTime serviceTime2=null;
		ProviderAccount provider2 = new ProviderAccount();
		MemberAccount member2 = new MemberAccount();
		String comments2 = "This is the second service.";
		LocalDateTime currentTime2=null;
		
		ServiceRecord serviceRecord2 = new ServiceRecord(serviceTime2,service2,provider2,member2,comments2,currentTime2);
		
		//Create third service to be added to the ServiceRecordController
		Service service3 = new Service(27,135791,"Third Service",222222,"John");
		LocalDateTime serviceTime3=null;
		ProviderAccount provider3 = new ProviderAccount();
		MemberAccount member3 = new MemberAccount();
		String comments3 = "This is the third service.";
		LocalDateTime currentTime3=null;
		
		ServiceRecord serviceRecord3 = new ServiceRecord(serviceTime3,service3,provider3,member3,comments3,currentTime3);
		
		
		//Create serviceRecordContoller and add the three serviceRecord objects to it
		ServiceRecordController serviceRecordController = new ServiceRecordController();
		serviceRecordController.createServiceRecord(serviceRecord1);
		serviceRecordController.createServiceRecord(serviceRecord2);
		serviceRecordController.createServiceRecord(serviceRecord3);
		
		//test getServiceRecordListSize() and test it with the anticipated value of 3
		assertEquals(3,serviceRecordController.getServiceRecordListSize());
	}
	
	
	
	
	@Test
	public void testSuccess2() {
		//Create first service to be added to the ServiceRecordController
		Service service1 = new Service(12,123456,"First Service",654321,"John");
		LocalDateTime serviceTime1=null;
		ProviderAccount provider1 = new ProviderAccount();
		MemberAccount member1 = new MemberAccount();
		String comments1 = "This is the first service.";
		LocalDateTime currentTime1=null;
		
		ServiceRecord serviceRecord1 = new ServiceRecord(serviceTime1,service1,provider1,member1,comments1,currentTime1);
		
		//Create second service to be added to the ServiceRecordController
		Service service2 = new Service(17,246802,"Second Service",101010,"Aaron");
		LocalDateTime serviceTime2=null;
		ProviderAccount provider2 = new ProviderAccount();
		MemberAccount member2 = new MemberAccount();
		String comments2 = "This is the second service.";
		LocalDateTime currentTime2=null;
		
		ServiceRecord serviceRecord2 = new ServiceRecord(serviceTime2,service2,provider2,member2,comments2,currentTime2);
		
		//Create third service to be added to the ServiceRecordController
		Service service3 = new Service(27,135791,"Third Service",222222,"Zack");
		LocalDateTime serviceTime3=null;
		ProviderAccount provider3 = new ProviderAccount();
		MemberAccount member3 = new MemberAccount();
		String comments3 = "This is the third service.";
		LocalDateTime currentTime3=null;
		
		ServiceRecord serviceRecord3 = new ServiceRecord(serviceTime3,service3,provider3,member3,comments3,currentTime3);
		
		
		//Create serviceRecordContoller and add the three serviceRecord objects to it
		ServiceRecordController serviceRecordController = new ServiceRecordController();
		serviceRecordController.createServiceRecord(serviceRecord1);
		serviceRecordController.createServiceRecord(serviceRecord2);
		serviceRecordController.createServiceRecord(serviceRecord3);
		
		//test if the correct ServiceRecord object is returned from getServiceRecord()
		assertEquals(serviceRecord3,serviceRecordController.getServiceRecord(2));
		
		//test if deleteServiceRecord() removes a desired serviceRecord and then test if remaining serviceRecord objects in list correctly fill in gap
		serviceRecordController.deleteServiceRecord(serviceRecord1);
		serviceRecordController.getServiceRecord(0);
		assertEquals(serviceRecord2,serviceRecordController.getServiceRecord(0));
	}
	
	
	
	
	@Test (expected = NullPointerException.class)
	public void testFailure() {
		
		//create three null ServiceRecord objects
		ServiceRecord serviceRecord1 = new ServiceRecord();
		ServiceRecord serviceRecord2 = new ServiceRecord();
		ServiceRecord serviceRecord3 = new ServiceRecord();
		
		//add the three ServiceRecord objects to the ServiceRecordController
		ServiceRecordController serviceRecordController = new ServiceRecordController();
		serviceRecordController.createServiceRecord(serviceRecord1);
		serviceRecordController.createServiceRecord(serviceRecord2);
		serviceRecordController.createServiceRecord(serviceRecord3);
		
		//test if calling the null provider data objects creates a NullPointerException as anticipated
		serviceRecordController.getServiceRecord(0).getProvider().getAccountEmail();
	}

}
