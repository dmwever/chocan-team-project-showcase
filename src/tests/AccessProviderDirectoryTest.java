//Written by David Wever (dmwever)
package tests;

import static org.junit.Assert.*;

import org.junit.*;

import cs200spring2018team3.ChocAnDatabase;
import cs200spring2018team3.Service;

public class AccessProviderDirectoryTest {
	ChocAnDatabase database = new ChocAnDatabase();
	Service service1 = new Service();
	Service service2 = new Service();
	
	
	
	@Test
	public void ProviderDirectoryTest() {
		service1.setName("Service1");
		service1.setCode(2333);
		service1.setDescription("1st");
		service2.setName("Service2");
		service2.setCode(2553);
		service2.setDescription("2nd");
		database.createService(service1);
		database.createService(service2);
		assertEquals("email to: JohnCena@gmail.com\nPROVIDER DIRECTORY\n\n\nService Name: Service1\nService Code: 2333\nService Description: 1st\n\n\nService Name: Service2\nService Code: 2553\nService Description: 2nd\n\n\n", database.displayProviderDirectory("JohnCena@gmail.com"));

	}
	
	@Test
	public void ProviderDirectoryTest2() {
		service1.setName("This is a test");
		service1.setCode(1831);
		service1.setDescription("of the UA weather alert systems");
		service2.setName("For God so loved the world");
		service2.setCode(316);
		service2.setDescription("He sent His only son");
		database.createService(service1);
		database.createService(service2);
		assertEquals("email to: JohnCena@gmail.com\nPROVIDER DIRECTORY\n\n\nService Name: This is a test\nService Code: 1831\nService Description: of the UA weather alert systems\n\n\nService Name: For God so loved the world\nService Code: 316\nService Description: He sent His only son\n\n\n", database.displayProviderDirectory("JohnCena@gmail.com"));

	}
	
	@Test (expected = NullPointerException.class)
	public void ProviderDirectoryFail() {
		service1 = null;
		service1.setName("This is a test");
		service1.setCode(1831);
		service1.setDescription("of the UA weather alert systems");
		database.createService(service1);
		assertEquals("email to: JohnCena@gmail.com\nPROVIDER DIRECTORY\n\n\nService Name: This is a test\nService Code: 1831\nService Description: of the UA weather alert systems\n\n\n", database.displayProviderDirectory("JohnCena@gmail.com"));

	}

}
