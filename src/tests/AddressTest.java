package tests;

import static org.junit.Assert.*;

import org.junit.*;

import cs200spring2018team3.Address;

public class AddressTest {

	@Test
	public void testObject() {
		Address address = new Address();
		address.setCity("Dothan");
		address.setState("AL");
		address.setStreet("1244 Pebblecreek Ln");
		address.setZipCode(36303);
		assertEquals("Dothan", address.getCity());
		assertEquals("AL", address.getState());
		assertEquals("1244 Pebblecreek Ln", address.getStreet());
		assertEquals(36303, address.getZipCode());
	}

	@Test
	public void testConstructorFull() {
		Address address = new Address("1244 Pebblecreek Ln", "Dothan", "AL", 36303);
		assertEquals("Dothan", address.getCity());
		assertEquals("AL", address.getState());
		assertEquals("1244 Pebblecreek Ln", address.getStreet());
		assertEquals(36303, address.getZipCode());
	}
	
	@Test
	public void testConstructorEmpty() {
		Address address = new Address();
		assertEquals("", address.getCity());
		assertEquals("", address.getState());
		assertEquals("", address.getStreet());
		assertEquals(00000, address.getZipCode());
	}
}
