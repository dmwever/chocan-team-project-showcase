package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.*;

import cs200spring2018team3.Address;
import cs200spring2018team3.MemberAccount;
import cs200spring2018team3.ProviderAccount;
import cs200spring2018team3.Service;
import cs200spring2018team3.ServiceRecord;

public class ServiceRecordTest {

	@Test
	public void testServiceRecord() {
		ServiceRecord serviceRecord = new ServiceRecord();
		Service service = new Service();
		Address address = new Address("1244 Pebblecreek Ln", "Dothan", "AL", 36303);
		ProviderAccount provider = new ProviderAccount("John Doe", 3, address, "Jdoe@joeshmoe.com");
		MemberAccount account = new MemberAccount("Bob Ross", 1776, address, "bross@gmail.com", false);
		LocalDateTime time = LocalDateTime.now();
		serviceRecord.setServiceDate(time);
		serviceRecord.setCurrentDateTime(time);
		serviceRecord.setService(service);
		serviceRecord.setProvider(provider);
		serviceRecord.setMember(account);
		serviceRecord.setComments("This is a test of the UA Weather Alerts System");
		assertEquals("This is a test of the UA Weather Alerts System", serviceRecord.getComments());
		assertEquals(time, serviceRecord.getCurrentDate());
		assertEquals(time, serviceRecord.getServiceDate());
		assertEquals(service, serviceRecord.getService());
		assertEquals(account, serviceRecord.getMember());
		assertEquals(provider, serviceRecord.getProvider());
		assertEquals("Dothan", serviceRecord.getMember().getAccountAddress().getCity());
		assertEquals(3, serviceRecord.getProvider().getAccountNumber());
	}
	@Test (expected = NullPointerException.class)
	public void failServiceRecord() {
		ServiceRecord serviceRecord = new ServiceRecord();
		Service service = new Service();
		ProviderAccount provider = null;
		MemberAccount account = null;
		LocalDateTime time = LocalDateTime.now();
		serviceRecord.setServiceDate(time);
		serviceRecord.setCurrentDateTime(time);
		serviceRecord.setService(service);
		serviceRecord.setProvider(provider);
		serviceRecord.setMember(account);
		serviceRecord.setComments("This is a test of the UA Weather Alerts System");
		assertEquals("This is a test of the UA Weather Alerts System", serviceRecord.getComments());
		assertEquals(time, serviceRecord.getCurrentDate());
		assertEquals(time, serviceRecord.getServiceDate());
		assertEquals(service, serviceRecord.getService());
		assertEquals(account, serviceRecord.getMember());
		assertEquals(provider, serviceRecord.getProvider());
		assertEquals("Dothan", serviceRecord.getMember().getAccountAddress().getCity());
		assertEquals(3, serviceRecord.getProvider().getAccountNumber());
	}
	
	@Test
	public void testServiceRecord2() {
		Service service = new Service();
		Address address = new Address("1244 Pebblecreek Ln", "Dothan", "AL", 36303);
		ProviderAccount provider = new ProviderAccount("John Doe", 3, address, "Jdoe@joeshmoe.com");
		MemberAccount account = new MemberAccount("Bob Ross", 1776, address, "bross@gmail.com", false);
		LocalDateTime time = LocalDateTime.now();
		ServiceRecord serviceRecord = new ServiceRecord(time, service, provider, account, "This is a test of the UA Weather Alerts System", time);
		assertEquals("This is a test of the UA Weather Alerts System", serviceRecord.getComments());
		assertEquals(time, serviceRecord.getCurrentDate());
		assertEquals(time, serviceRecord.getServiceDate());
		assertEquals(service, serviceRecord.getService());
		assertEquals(account, serviceRecord.getMember());
		assertEquals(provider, serviceRecord.getProvider());
		assertEquals("Dothan", serviceRecord.getMember().getAccountAddress().getCity());
		assertEquals(3, serviceRecord.getProvider().getAccountNumber());
	}
}
