package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

public class MemberAccount extends Account {
	private boolean accountSuspension;

	public MemberAccount() {
		super();
		accountSuspension = false;
	}

	public MemberAccount(String name, int number, Address address, String email, boolean suspended) {
		super(name, number, address, email);
		accountSuspension = suspended;
	}

	public boolean getSuspension() {
		return accountSuspension;
	}

	public void setSuspension(boolean suspended) {
		accountSuspension = suspended;
		return;
	}
}
