package cs200spring2018team3;

import java.time.LocalDateTime;
/**
 * The TerminalProvider class. This gives providers an interface to find/bill customers.
 * Providers can also access the provider directory.
 * @author jt1du
 *
 */
public class TerminalProvider {
	private ChocAnDatabase database;
	private ProviderAccount account;



	public TerminalProvider(ChocAnDatabase database2, Account account2) {
		database = database2;
		account = (ProviderAccount)account2;
	}

	public boolean verifyMember(int idNumber) {
		MemberAccount member = (MemberAccount) database.getAccount(idNumber);
		if(member != null)
			return !member.getSuspension();
		else
			return false;
	}
	/**
	 * Searches members in the database based on their name.
	 * @param name - Member name
	 * @return	- Member profile and suspension status
	 */
	public boolean verifyMember(String name) {
		MemberAccount member = (MemberAccount) database.getAccount(name);
		if(member != null)
			return member.getSuspension();
		else
			return false;


	}
	/**
	 * Sets the service record information with current date/time to bill the member later
	 * @param idNumber - Member 9-Digit ID
	 * @param serviceCode - 6 Digit Service Code
	 * @param date - Local Date/Time
	 */
	public void billMember(int idNumber, String serviceCode, LocalDateTime date, String comment) {
		ServiceRecord service = new ServiceRecord();
		service.setMember((MemberAccount)database.getAccount(idNumber));
		service.setService(database.getService(serviceCode));
		service.setProvider(account);
		LocalDateTime currentTime = LocalDateTime.now();
		service.setCurrentDateTime(currentTime);
		service.setServiceDate(date);
		service.setComments(comment);
	}
	/**
	 * Sets the service record information with entered date/time to bill the member later
	 * @param idNumber - Member 9-Digit ID
	 * @param serviceCode - 6 Digit Service Code
	 * @param year - Entered Year
	 * @param month - Entered Month
	 * @param day - Entered Day
	 * @param hour - Entered Hour
	 * @param minute - Entered Minute
	 * @param comment
	 */
	public void billMember(int idNumber, String serviceCode, int year, int month, int day, int hour, int minute, String comment) {
		ServiceRecord service = new ServiceRecord();
		service.setMember((MemberAccount) database.getAccount(idNumber));
		service.setService(database.getService(serviceCode));
		service.setProvider(account);

		LocalDateTime date = LocalDateTime.of(year, month, day, hour, minute);
		service.setServiceDate(date);

		LocalDateTime currentTime = LocalDateTime.now();
		service.setCurrentDateTime(currentTime);

		service.setComments(comment);
		database.createServiceRecord(service);
	}
	/**
	 * Allows the provider to display the provider directory.
	 */
	public String getProviderDirectory() {
		String email;
		if(account != null)
			email = account.getAccountEmail();
		else
			email = "";
		return database.displayProviderDirectory(email);
	}


}
