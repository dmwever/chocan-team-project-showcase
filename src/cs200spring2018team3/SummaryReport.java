package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class SummaryReport extends Report {


	public SummaryReport(ServiceRecordController serviceRecordList) {
		super(serviceRecordList);
	}

	public void printReport() {
		ServiceRecord service;
		ArrayList<ProviderAccount> providerAccounts = new ArrayList<>();
		int providerConsultations = 0;
		int numConsultations = 0;
		double totalFee = 0;
		double providerFee = 0;
		// Crawl through the Service Record List and add unique providers to providerAccount list
	    for (int i=0; i<serviceRecordList.getServiceRecordListSize();i++) {
	      service = serviceRecordList.getServiceRecord(i);
	      for (int j=0; j<providerAccounts.size(); ++j){
	    		if (service.getProvider().getAccountName() == providerAccounts.get(i).getAccountName()) break;
	    		if (j == providerAccounts.size()-1) providerAccounts.add(service.getProvider());
	      }
	    }

	    PrintWriter writer = null;
		try {
			writer = new PrintWriter("SummaryReport.txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write("Summary Repoprt:\n");

	    for (int i=0; i<providerAccounts.size(); ++i) {
			for (int j=0; j<providerAccounts.size(); ++j){
		      service = serviceRecordList.getServiceRecord(j);
		      if (service.getProvider().getAccountName() == providerAccounts.get(i).getAccountName()) {
		    	  ++providerConsultations;
		    	  providerFee = service.getService().getFee();
		    	  ++numConsultations;
		    	  totalFee += service.getService().getFee();
		      }
		   }
		   writer.write("\nProvider Name:             ");
		   writer.write(providerAccounts.get(i).getAccountName());
		   writer.write("\nNumber of Consultations:   ");
		   writer.write(providerConsultations);
		   writer.write("\nTotal Provider Fee:        ");
		   writer.write((int) providerFee);
		   providerFee = 0;
		   providerConsultations = 0;
	    }

	    writer.write("\n\nTotal Number of Providers:           ");
	    writer.write(providerAccounts.size());
	    writer.write("\nTotal Number of Consultations:       ");
	    writer.write(numConsultations);
	    writer.write("\nTotal Fees:                          ");
	    writer.write((int)totalFee);
	}
}
