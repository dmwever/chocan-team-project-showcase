package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.format.DateTimeFormatter;

public class ProviderReport extends Report {
	private ProviderAccount account;

	public ProviderReport(ProviderAccount account, ServiceRecordController serviceRecordList) {
		super(serviceRecordList);
		this.account = account;
	}

	public ProviderReport() {
		super();
		account = null;
	}


	public void printReport() {
		float totalFee = 0;
		int totalConsultations = 0;

		PrintWriter writer = null;
		try {
			writer = new PrintWriter(account.getAccountName() + ".txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		writer.write("Provider name:		");
		writer.write(account.getAccountName());
		writer.write("\nProvider ID:		");
		writer.write(""+account.getAccountNumber());
		Address address = account.getAccountAddress();
		writer.write("\nProvider Address:	");
		writer.write(address.getStreet());
		writer.write("\nProvider City:		");
		writer.write(address.getCity());
		writer.write("\nProvider State:		");
		writer.write(address.getState());
		writer.write("\nProvider Zip:		");
		writer.write(""+address.getZipCode());

		writer.write("\n\nMembers served this week:\n\n");
		int i;
		for (i = 0; i < serviceRecordList.getServiceRecordListSize(); i++) {
			if (account.getAccountName() == serviceRecordList.getServiceRecord(i).getProvider().getAccountName()) {
			  writer.write("	Member Name:					" + serviceRecordList.getServiceRecord(i).getMember().getAccountName() + "\n");
			  writer.write("	Member ID:						" + serviceRecordList.getServiceRecord(i).getMember().getAccountNumber() + "\n");
			  writer.write("	Service Code:					" + serviceRecordList.getServiceRecord(i).getService().getCode() + "\n");
			  writer.write("	Time of Service:				" + serviceRecordList.getServiceRecord(i).getServiceDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy")) + "\n");
			  writer.write("	Time of Service Documentation:	" + serviceRecordList.getServiceRecord(i).getCurrentDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss")) + "\n");
			  totalFee += serviceRecordList.getServiceRecord(i).getService().getFee();
			  totalConsultations++;
			}
		}
		writer.write("\nTotalFee:			" + totalFee);
		writer.write("\nTotalConsultations:	" + totalConsultations);
		writer.close();
	}
}
/* Include service date, time data was received, Member name,
 * member number, service code, fee to be paid. Also include total
 *  consultations and total fee for the week.
 */