package cs200spring2018team3;

/**
 * Description: ServiceRecord class. This class contains the structure for a service record. This includes the service date,
 * 				the service, the provider, the member, comments about the service, and the time of making the service record.
 * 				This class also contains methods for setting and getting individual details about a service record.
 * @author Daniel Michaels
 * 
 */

import java.time.LocalDateTime;

public class ServiceRecord {
	private LocalDateTime serviceDateTime;
	private Service service;
	private ProviderAccount provider;
	private MemberAccount member;
	private String comments;
	private LocalDateTime currentDateTime;

	
	public ServiceRecord() {
		serviceDateTime = null;
		service = null;
		provider = null;
		member = null;
		comments = "";
		currentDateTime = null;
	}
	
	public ServiceRecord(LocalDateTime serviceDateTime, Service service, ProviderAccount provider, MemberAccount member,
			String comments, LocalDateTime currentDateTime) {
		super();
		this.serviceDateTime = serviceDateTime;
		this.service = service;
		this.provider = provider;
		this.member = member;
		this.comments = comments;
		this.currentDateTime = currentDateTime;
		//constructs the entire service record object when all parameters are passed in
	}

	public LocalDateTime getServiceDate() {
		return serviceDateTime;
		//returns the date of the service
	}
	public void setServiceDate(LocalDateTime date) {
		serviceDateTime = date;
		//individually sets the date of the service
	}
	
	public Service getService() {
		return service;
		//returns the service object of a service record
	}
	public void setService(Service service) {
		this.service = service;
		//individually sets the service associated in a report
	}

	public ProviderAccount getProvider() {
		return provider;
		//returns the provider connected to a service report
	}
	public void setProvider(ProviderAccount provider) {
		this.provider = provider;
		//individually sets the provider of a report
	}

	public MemberAccount getMember() {
		return member;
		//returns the member connected to a service report
	}
	public void setMember(MemberAccount member) {
		this.member = member;
		//sets the member of a report
	}
	
	public String getComments() {
		return comments;
		//returns the comments of a report
	}
	public void setComments(String comms) {
		comments = comms;
		//sets the comments of a report
	}
	
	public LocalDateTime getCurrentDate() {
		return currentDateTime;
		//returns the date the report was created
	}
	public void setCurrentDateTime(LocalDateTime date) {
		currentDateTime = date;
		//sets the creation date of the report
	}
	
	//No longer need setCurrentTime or getCurrentTime
}
