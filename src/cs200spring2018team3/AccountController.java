package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

public class AccountController {
	private ArrayList<Account> accountList;

	public AccountController() {
		accountList = new ArrayList<Account>();
	}

	public Account getAccount(int idNumber) {
		for (int i=0;i<accountList.size();i++)
		{
			if (accountList.get(i).getAccountNumber()==idNumber)
			{
				return accountList.get(i);
			}
		}
		return null;
	}



	public Account getAccount(String name) {
		for (int i=0;i<accountList.size();i++)
		{
			if (accountList.get(i).getAccountName().equals(name))
			{
				return accountList.get(i);
			}
		}
		return null;
	}



	public void createAccount(Account newAccount) {
		accountList.add(newAccount);
	}



	public void removeAccount(String name) {
		for (int i=0;i<accountList.size();i++)
		{
			if (accountList.get(i).getAccountName().equals(name))
			{
				accountList.remove(i);
			}
		}
	}



	public void removeAccount(int idNumber) {
		/*
		for(Account a : accountList) {

		}*/
		for (int i=0;i<accountList.size();i++)
		{
			if (accountList.get(i).getAccountNumber()==idNumber)
			{
				accountList.remove(i);
			}
		}
	}



	public void write() {
		File dir = new File("accounts");
		if(!dir.isDirectory())
			dir.mkdirs();
		else
			for(File f : dir.listFiles())
				f.delete();
		for(Account a : accountList) {
			try {
				PrintStream s =  new PrintStream("accounts\\"+a.getAccountName()+".txt");
				if(a instanceof MemberAccount) {
					s.println("member");
					s.println(""+((MemberAccount) a).getSuspension());
				}
				else {
					s.println("provider");
				}
				s.println(a.getAccountName());
				s.println(a.getAccountEmail());
				s.println(a.getAccountAddress());
				s.println(""+a.getAccountNumber());
				s.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}
