package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.format.DateTimeFormatter;

public class MemberReport extends Report {
	private MemberAccount account;

	public MemberReport(MemberAccount account,ServiceRecordController serviceRecordController) {
		super(serviceRecordController);
		this.account = account;
	}
	public MemberReport() {
		account = null;
	}

	public void printReport() {

		PrintWriter writer = null;
		try {
			writer = new PrintWriter(account.getAccountName() + ".txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		writer.write("Member name:		");
		writer.write(account.getAccountName());
		writer.write("\nMember ID:			");
		writer.write(""+account.getAccountNumber());
		Address address = account.getAccountAddress();
		writer.write("\nMember Address:		");
		writer.write(address.getStreet());
		writer.write("\nMember City:		");
		writer.write(address.getCity());
		writer.write("\nMember State:		");
		writer.write(address.getState());
		writer.write("\nMember Zip:			");
		writer.write(address.getZipCode());

		writer.write("\n\nYour services this week:\n\n");
		int i;
		for (i = 0; i < serviceRecordList.getServiceRecordListSize(); i++) {
			writer.write("	" + serviceRecordList.getServiceRecord(i).getService().getName());
			writer.write("	" + serviceRecordList.getServiceRecord(i).getServiceDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy")) + "\n");
			writer.write("	" + serviceRecordList.getServiceRecord(i).getProvider().getAccountName() + "\n\n");
		}
		writer.close();
	}

}
/** Per member, provide each member name, number, address, city, state,
 *  and zip, and a list of each service including the service date,
 *  provider name, and service name. Then e-mail the report to the
 *   provider.
 */