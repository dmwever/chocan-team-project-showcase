package cs200spring2018team3;

/**
 * Description: ServiceRecordController class. This class contains the list of service records that were added to the database.
 * 				This allows for recalling service records by passing its index, returning the number of service records in
 * 				the service record list, adding a new service record to the list, deleting a service record by either providing
 * 				its index or serviceRecord object, and print the list of service records along with their information.
 * @author Daniel Michaels
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ServiceRecordController {
	private ArrayList<ServiceRecord> serviceRecordList;

	public ServiceRecordController(ArrayList<ServiceRecord> serviceRecordList) {
		super();
		this.serviceRecordList = serviceRecordList;
		//create new serviceRecordList by passing in an existing ServiceRecord ArrayList
	}
	public ServiceRecordController() {
		super();
		serviceRecordList = new ArrayList<ServiceRecord>();
		//default constructor
	}

	public ServiceRecord getServiceRecord(int spot) {
		return serviceRecordList.get(spot);
		//return a service record in the serviceRecordList based on the spot passed into the method
	}
	public int getServiceRecordListSize() {
		return serviceRecordList.size();
		//returns the number of serviceRecords in the serviceRecordList
	}
	public void createServiceRecord(ServiceRecord newServiceRecord) {
		serviceRecordList.add(newServiceRecord);
		//adds the newServiceRecord parameter into the serviceRecordList
	}
	public void deleteServiceRecord(ServiceRecord oldServiceRecord) {
		int index = serviceRecordList.indexOf(oldServiceRecord);
		serviceRecordList.remove(index);
		//deletes the ServiceRecord that was passed in through the parameter from the serviceRecordList
	}
	public void deleteServiceRecord(int spot) {
		serviceRecordList.remove(spot);
		//deletes the ServiceRecord at the specified index "spot" in serviceRecordList
	}
	public void write() {
		File dir = new File("servicerecords");
		if(!dir.isDirectory())
			dir.mkdirs();
		else
			for(File f : dir.listFiles())
				f.delete();
		for(ServiceRecord a : serviceRecordList) {
			try {
				PrintStream s =  new PrintStream("servicerecords\\"+a.getService().getName()+".txt");
				s.println(""+a.getCurrentDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss")));
				s.println(""+a.getServiceDate().format(DateTimeFormatter.ofPattern("MM-dd-yyyy")));
				s.println(""+a.getProvider().getAccountNumber());
				s.println(""+a.getMember().getAccountNumber());
				s.println(""+a.getService().getCode());
				s.println(a.getComments());


				s.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		//prints each service record in serviceRecordList along with each service record's information
	}
}
