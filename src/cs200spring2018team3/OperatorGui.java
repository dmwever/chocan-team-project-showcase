package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class OperatorGui extends GUI{
	private final TerminalOperator terminal;
	private Stage stage;

	private EventHandler<ActionEvent> addMemberFunction,addProviderFunction,removeMemberFunction,removeProviderFunction,editMemberFunction,editProviderFunction;



	public OperatorGui(ChocAnDatabase database) {
		terminal = new TerminalOperator(database);

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));


		ArrayList<TextField> fields = new ArrayList<>();
		TextField name = new TextField();
		grid.add(name, 0, 5);
		name.setVisible(false);
		name.setPromptText("Name");
		fields.add(name);
		TextField email = new TextField();
		grid.add(email, 0, 6);
		email.setVisible(false);
		email.setPromptText("Email");
		fields.add(email);
		TextField street = new TextField();
		grid.add(street, 0, 7);
		street.setVisible(false);
		street.setPromptText("Street address");
		fields.add(street);
		TextField city = new TextField();
		grid.add(city, 0, 8);
		city.setVisible(false);
		city.setPromptText("City");
		fields.add(city);
		TextField state = new TextField();
		grid.add(state, 0, 9);
		state.setVisible(false);
		state.setPromptText("State");
		fields.add(state);
		TextField zip = new TextField();
		grid.add(zip, 0, 10);
		zip.setVisible(false);
		zip.setPromptText("Zip code");
		fields.add(zip);
		TextField id = new TextField();
		id.setPromptText("ID number");
		id.setVisible(false);
		grid.add(id, 0, 4);
		Button enter = new Button("Enter");
		enter.setVisible(false);
		grid.add(enter, 1, 4);

		ArrayList<Button> buttons = new ArrayList<Button>();
		Button addMember = new Button("Add Member");
		addMember.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				for(Node t : fields) {
					t.setVisible(true);
				}
				enter.setVisible(true);
				id.setVisible(false);
				enter.setOnAction(addMemberFunction);
				for(Button b : buttons) {
					b.setDisable(true);
				}
				addMember.setDefaultButton(true);
			}

		});
		grid.add(addMember, 1, 1);
		buttons.add(addMember);

		Button addProvider = new Button("Add Provider");
		addProvider.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				for(Node t : fields) {
					t.setVisible(true);
				}
				id.setVisible(false);
				enter.setVisible(true);
				enter.setOnAction(addProviderFunction);
				for(Button b : buttons) {
					b.setDisable(true);
				}
				addProvider.setDefaultButton(true);
			}

		});
		grid.add(addProvider, 0, 1);
		buttons.add(addProvider);

		Button removeMember = new Button("Remove Member");
		removeMember.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				for(Node t : fields) {
					t.setVisible(false);
				}
				name.setVisible(true);
				id.setVisible(true);
				enter.setVisible(true);
				enter.setOnAction(removeMemberFunction);
				for(Button b : buttons) {
					b.setDisable(true);
				}
				removeMember.setDefaultButton(true);
			}
		});
		grid.add(removeMember, 1, 2);
		buttons.add(removeMember);

		Button removeProvider = new Button("Remove Provider");
		removeProvider.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				for(Node t : fields) {
					t.setVisible(false);
				}
				name.setVisible(true);
				id.setVisible(true);
				enter.setVisible(true);
				enter.setOnAction(removeProviderFunction);
				for(Button b : buttons) {
					b.setDisable(true);
				}
				removeProvider.setDefaultButton(true);
			}
		});
		grid.add(removeProvider, 0, 2);
		buttons.add(removeProvider);

		Button editMember = new Button("Edit Member");
		editMember.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				for(Node t : fields) {
					t.setVisible(true);
				}
				id.setVisible(true);
				enter.setVisible(true);
				enter.setOnAction(editMemberFunction);
				for(Button b : buttons) {
					b.setDisable(true);
				}
				editMember.setDefaultButton(true);
			}
		});
		grid.add(editMember, 1, 3);
		buttons.add(editMember);

		Button editProvider = new Button("Edit Provider");
		editProvider.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				for(Node t : fields) {
					t.setVisible(true);
				}
				id.setVisible(true);
				enter.setVisible(true);
				enter.setOnAction(editProviderFunction);
				for(Button b : buttons) {
					b.setDisable(true);
				}
				editProvider.setDefaultButton(true);
			}
		});
		grid.add(editProvider, 0, 3);
		buttons.add(editProvider);

		stage = new Stage();
		stage.setScene(new Scene(grid));

		addMemberFunction = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				System.out.println(terminal.addMember(name.getText(), street.getText(), city.getText(), state.getText(), zip.getText(), email.getText()));
				for(TextField t : fields) {
					t.setVisible(false);
					t.clear();
				}
				enter.setVisible(false);
				for(Button b : buttons) {
					b.setDisable(false);
				}
				addMember.setDefaultButton(false);
			}

		};

		addProviderFunction = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				System.out.println(terminal.addProvider(name.getText(), street.getText(), city.getText(), state.getText(), zip.getText(), email.getText()));
				for(TextField t : fields) {
					t.setVisible(false);
					t.clear();
				}
				enter.setVisible(false);
				for(Button b : buttons) {
					b.setDisable(false);
				}
				addProvider.setDefaultButton(false);
			}

		};

		removeMemberFunction = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if(!id.getText().equals("")) {
					System.out.println(terminal.deleteMember(Integer.parseInt(id.getText())));
				}
				else if(!name.getText().equals("")) {
					System.out.println(terminal.deleteMember(name.getText()));
				}
				id.setVisible(false);
				id.clear();
				name.setVisible(false);
				name.clear();
				enter.setVisible(false);
				for(Button b : buttons) {
					b.setDisable(false);
				}
				removeMember.setDefaultButton(false);
			}

		};

		removeProviderFunction = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if(!id.getText().equals("")) {
					System.out.println(terminal.deleteProvider(Integer.parseInt(id.getText())));
				}
				else if(!name.getText().equals("")) {
					System.out.println(terminal.deleteProvider(name.getText()));
				}
				id.setVisible(false);
				id.clear();
				name.setVisible(false);
				name.clear();
				enter.setVisible(false);
				for(Button b : buttons) {
					b.setDisable(false);
				}
				removeProvider.setDefaultButton(false);

			}

		};

		editMemberFunction = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println(terminal.updateMember(Integer.parseInt(id.getText()), name.getText(), street.getText(), city.getText(), state.getText(), zip.getText(), email.getText()));
				for(TextField t : fields) {
					t.setVisible(false);
					t.clear();
				}
				id.setVisible(false);
				id.clear();
				name.setVisible(false);
				name.clear();
				enter.setVisible(false);
				for(Button b : buttons) {
					b.setDisable(false);
				}
				editMember.setDefaultButton(false);
			}

		};

		editProviderFunction = new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.out.println(terminal.updateProvider(Integer.parseInt(id.getText()), name.getText(), street.getText(), city.getText(), state.getText(), zip.getText(), email.getText()));
				for(TextField t : fields) {
					t.setVisible(false);
					t.clear();
				}
				id.setVisible(false);
				id.clear();
				name.setVisible(false);
				name.clear();
				enter.setVisible(false);
				for(Button b : buttons) {
					b.setDisable(false);
				}
				editProvider.setDefaultButton(false);
			}
		};

	}

	public void open() {
		stage.show();

	}

	@Override
	public void close() {
		stage.close();

	}

}
