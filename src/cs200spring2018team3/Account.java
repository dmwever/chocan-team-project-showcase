package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

abstract public class Account {
	private String accountName;
	private int accountNumber;
	private Address accountAddress;
	private String accountEmail;
	private static int currentNumber = 0;

	public Account() {
		accountName = "";
		accountNumber = currentNumber++;
		accountAddress = null;
		accountEmail = "";
	}

	public Account(String name, int number, Address address, String email) {
		accountName = name;
		accountNumber = number;
		accountAddress = address;
		accountEmail = email;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String name) {
		accountName = name;
		return;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int number) {
		accountNumber = number;
		return;
	}

	public Address getAccountAddress() {
		return accountAddress;
	}

	public void setAccountAddress(Address address) {
		accountAddress = address;
		return;
	}

	public String getAccountEmail() {
		return accountEmail;
	}

	public void setAccountEmail(String email) {
		accountEmail = email;
		return;
	}

	static public void setIdSpot(int i) {
		currentNumber = i;
	}
}
