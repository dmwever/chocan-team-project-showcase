package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

abstract public class Report {

	protected ServiceRecordController serviceRecordList;

	public Report(ServiceRecordController serviceRecordList) {
		super();
		this.serviceRecordList = serviceRecordList;
	}
	public Report() {
		serviceRecordList = null;
	}

	abstract public void printReport();
	public void addServiceRecord(ServiceRecord record) {
		serviceRecordList.createServiceRecord(record);
	}
}
