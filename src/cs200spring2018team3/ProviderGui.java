package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ProviderGui extends GUI{
	private TerminalProvider terminal;
	private Stage stage;
	public ProviderGui(ChocAnDatabase database) {
		ProviderAccount account;
		String msg = JOptionPane.showInputDialog("Enter name or idNumber");
		try {
			account = (ProviderAccount) database.getAccount(Integer.parseInt(msg));
		}
		catch(NumberFormatException e) {
			try {
				account = (ProviderAccount) database.getAccount(msg);
			}
			catch(ClassCastException f) {
				account = null;
			}
		}
		catch(ClassCastException e) {
			account = null;

		}
		if(account == null) {
			stage = new Stage();
			Scene scene = new Scene(new Label("Invalid account try again"));
			stage.setScene(scene);
			stage.show();
			return;
		}
		terminal = new TerminalProvider(database,account);

		GridPane root = new GridPane();

		Label output = new Label();
		root.add(output, 0, 0);

		TextField memberID = new TextField();
		memberID.setPromptText("member id");
		root.add(memberID, 0, 1);

		Button validateMember = new Button("Validate Member");
		validateMember.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				boolean valid = terminal.verifyMember(Integer.parseInt(memberID.getText()));
				String msg = valid == true ? "valid" : "invalid";
				output.setText("Member id is " + msg);

			}

		});
		root.add(validateMember, 1, 1);

		TextField serviceCode = new TextField();
		serviceCode.setPromptText("service code");
		root.add(serviceCode, 0, 2);
		TextField serviceMonth = new TextField();
		serviceMonth.setPromptText("service month");
		root.add(serviceMonth, 0, 3);
		TextField serviceDay = new TextField();
		serviceDay.setPromptText("service day");
		root.add(serviceDay, 0, 4);
		TextField serviceYear = new TextField();
		serviceYear.setPromptText("service year");
		root.add(serviceYear, 0, 5);
		TextField serviceHour = new TextField();
		serviceHour.setPromptText("service hour");
		root.add(serviceHour, 0, 6);
		TextField serviceMinute = new TextField();
		serviceMinute.setPromptText("service minute");
		root.add(serviceMinute, 0, 7);
		TextField comment = new TextField();
		comment.setPromptText("comments");
		root.add(comment, 0, 8);
		Button billMember = new Button("Bill Member");
		billMember.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				terminal.billMember(Integer.parseInt(memberID.getText()), serviceCode.getText(), Integer.parseInt(serviceYear.getText()), Integer.parseInt(serviceMonth.getText()), Integer.parseInt(serviceDay.getText()), Integer.parseInt(serviceHour.getText()), Integer.parseInt(serviceMinute.getText()),comment.getText());
			}

		});
		root.add(billMember, 1, 2);

		Button providerDirectory = new Button("Access Provider Directory");
		providerDirectory.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				Label label = new Label(terminal.getProviderDirectory());
				ScrollPane window = new ScrollPane();
				window.setContent(label);
				Stage wind = new Stage();
				wind.setScene(new Scene(window));
				wind.show();
			}

		});
		root.add(providerDirectory, 1, 3);

		Scene scene = new Scene(root);
		stage = new Stage();
		stage.setScene(scene);

	}

	public void open() {
		stage.show();
	}

	@Override
	public void close() {
			stage.close();
	}
}
