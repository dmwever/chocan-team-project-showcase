package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import javax.swing.text.DateFormatter;

public class ChocAnDatabase {
	private ServiceRecordController serviceRecordList;
	private AccountController accountList;
	private ServiceController serviceList;

	public ChocAnDatabase() {
		serviceRecordList = new ServiceRecordController();
		accountList = new AccountController();
		serviceList = new ServiceController();
		int i = -1;
		File dir = new File(".\\accounts");
		File[] files = dir.listFiles();
		if(files != null) {
		for(File f : files) {
			try {
				Scanner s = new Scanner(f);
				String msg = s.nextLine();
				Account a;
				if(msg.equalsIgnoreCase("provider")) {
					a = new ProviderAccount();

				}
				else {
					a = new MemberAccount();
					((MemberAccount)a).setSuspension(Boolean.parseBoolean(s.nextLine()));
				}
				a.setAccountName(s.nextLine());
				a.setAccountEmail(s.nextLine());
				Address address = new Address(s.nextLine(),s.nextLine(),s.nextLine(),Integer.parseInt(s.nextLine()));
				a.setAccountAddress(address);
				int temp = Integer.parseInt(s.nextLine());
				if(i < temp)
					i = temp;
				a.setAccountNumber(temp);
				accountList.createAccount(a);
				s.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		}
		Account.setIdSpot(i+1);
		dir = new File(".\\services");
		files = dir.listFiles();
		if(files != null) {
		for(File f : files) {
			try {
				Scanner s = new Scanner(f);
				Service service = new Service();
				service.setName(s.nextLine());
				service.setCode(Integer.parseInt(s.nextLine()));
				service.setProviderID(Integer.parseInt(s.nextLine()));
				service.setFee(Double.parseDouble(s.nextLine()));
				service.setDescription(s.nextLine());
				serviceList.createService(service);
				s.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		}
		dir = new File(".\\servicerecords");
		files = dir.listFiles();
		if(files != null) {
		for(File f : files) {
			try {
				Scanner s = new Scanner(f);
				ServiceRecord record = new ServiceRecord();
				record.setCurrentDateTime(LocalDateTime.parse(s.nextLine(),DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss")));
				record.setServiceDate(LocalDateTime.of(LocalDate.parse(s.nextLine(),DateTimeFormatter.ofPattern("MM-dd-yyyy")), LocalTime.of(0, 0)));
				record.setProvider((ProviderAccount)accountList.getAccount(Integer.parseInt(s.nextLine())));
				record.setMember((MemberAccount) accountList.getAccount(Integer.parseInt(s.nextLine())));
				record.setService(serviceList.getService((s.nextLine())));
				record.setComments(s.nextLine());
				serviceRecordList.createServiceRecord(record);
				s.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		}

	}

	public ServiceRecordController getServiceRecordList() {
		return serviceRecordList;
	}
	public Account getAccount(int idNumber) {
		return accountList.getAccount(idNumber);
	}
	public Account getAccount(String name) {
		return accountList.getAccount(name);
	}
	public void createAccount(Account newAccount) {
		accountList.createAccount(newAccount);
	}
	public void createService(Service newService) {
		serviceList.createService(newService);
	}
	public void createServiceRecord(ServiceRecord newServiceRecord) {
		serviceRecordList.createServiceRecord(newServiceRecord);
	}
	public void deleteAccount(int idNumber) {
		if(accountList.getAccount(idNumber) instanceof ProviderAccount)
			serviceList.deleteServices(idNumber);
		accountList.removeAccount(idNumber);
	}
	public void deleteAccount(String name) {
		if(accountList.getAccount(name) instanceof ProviderAccount)
			serviceList.deleteServices(accountList.getAccount(name).getAccountNumber());
		accountList.removeAccount(name);
	}

	public String displayProviderDirectory(String email) {
		String s = new String();
		System.out.println("email to: " + email);
		s += "email to: " + email;
		System.out.println();
		s += '\n';
		System.out.println("PROVIDER DIRECTORY");
		s += "PROVIDER DIRECTORY";
		System.out.println();
		System.out.println();
		System.out.println();
		s += '\n';
		s += '\n';
		s += '\n';

		for (int i=0; i<serviceList.getServiceListSize(); i++) {
			Service thisService = serviceList.getService(i);
			System.out.println("Service Name: " + thisService.getName());
			s += "Service Name: " + thisService.getName() + "\n";
			System.out.println("Service Code: " + thisService.getCode());
			s += "Service Code: " + thisService.getCode() + "\n";
			System.out.println("Service Description: " + thisService.getDescription());
			s += "Service Description: " + thisService.getDescription() + "\n";
			System.out.println();
			System.out.println();
			s += '\n';
			s += '\n';
		}
		return s;

	}



	public boolean findAccount(int idNumber) {
		if (accountList.getAccount(idNumber)==null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public boolean findAccount(String name) {
		if (accountList.getAccount(name)==null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public void closeSystem() {
		accountList.write();
		serviceList.write();
		serviceRecordList.write();
	}

	public Service getService(String serviceCode) {
		return serviceList.getService(serviceCode);
	}
}
