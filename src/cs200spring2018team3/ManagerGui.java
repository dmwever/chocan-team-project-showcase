package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ManagerGui extends GUI{
	private Stage stage;
	private final TerminalManagerTimer terminal;
	public ManagerGui(ChocAnDatabase database) {
		terminal = new TerminalManagerTimer(database);

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		TextField name = new TextField();
		grid.add(name, 0, 2);
		name.setPromptText("Enter name");
		name.setVisible(false);

		Button enter = new Button("Enter");
		enter.setVisible(false);
		grid.add(enter, 1, 2);

		Button providerReport = new Button("Run Provider Report");
		providerReport.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				name.setVisible(true);
				enter.setVisible(true);
				enter.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						terminal.createProviderReport(name.getText());
						name.setVisible(false);
						enter.setVisible(false);
					}
				});

			}

		});
		grid.add(providerReport, 0, 0);

		Button memberReport = new Button("Run Member Report");
		memberReport.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				name.setVisible(true);
				enter.setVisible(true);
				enter.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						terminal.createMemberReport(name.getText());
						name.setVisible(false);
						enter.setVisible(false);
					}
				});
			}

		});
		grid.add(memberReport, 1, 0);

		Button summaryReport = new Button("Run Summary Report");
		summaryReport.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				terminal.createSummaryReport();
			}

		});
		grid.add(summaryReport, 0, 1);



		Scene scene = new Scene(grid);
		stage = new Stage();
		stage.setScene(scene);

	}

	public void open() {
		// TODO Auto-generated method stub
		stage.show();
	}

	public void close() {
		stage.close();
	}

}
