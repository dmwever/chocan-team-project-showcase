package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainClass extends Application{
	private ArrayList<GUI> windows;

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage stage) throws Exception {
		windows = new ArrayList<GUI>();
		ChocAnDatabase database = new ChocAnDatabase();
		//Parent root = FXMLLoader.load(getClass().getResource("Main Window.fxml"));
		GridPane root = new GridPane();
		root.setAlignment(Pos.CENTER);
		root.setHgap(10);
		root.setVgap(10);
		root.setPadding(new Insets(25, 25, 25, 25));
		Button providerLogin = new Button();
		providerLogin.setText("Provider Login");
		providerLogin.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				ProviderGui provider = new ProviderGui(database);
				provider.open();
				windows.add(provider);
			}

		}
		);
		root.add(providerLogin, 0, 0);

		Button operatorLogin = new Button();
		operatorLogin.setText("Operator Login");
		operatorLogin.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				OperatorGui operator = new OperatorGui(database);
				operator.open();
				windows.add(operator);
			}

		}
		);
		root.add(operatorLogin,0,1);

		Button managerLogin = new Button();
		managerLogin.setText("Manager Login");
		managerLogin.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				ManagerGui manager = new ManagerGui(database);
				manager.open();
				windows.add(manager);
			}

		}
		);
		root.add(managerLogin,0,2);

		Scene scene = new Scene(root,500,500);
		stage.setScene(scene);
		stage.show();
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				database.closeSystem();
				for(GUI window : windows) {
					window.close();
				}

			}

		});

	}

}
