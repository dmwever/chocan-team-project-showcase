package cs200spring2018team3;

/**
 * Description: ServiceController class. This class contains the list of services that were added to the database.
 * 				This allows for an addition of a new service, the recall of a service through its index, removing a service
 * 				by providing its index or service object, provide the number of services, and print the services and their information.
 * @author Daniel Michaels
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

public class ServiceController {
	private ArrayList<Service> serviceList;

	public ServiceController() {
		serviceList = new ArrayList<Service>();
	}

	public Service getService(int number) {
		return serviceList.get(number);
		//returns the service at the specified index "number"
	}

	public Service getService(String code) {
		int serCode = Integer.parseInt(code);
		for(Service s: serviceList)
			if(s.getCode()==serCode)
				return s;
		return null;
	}
	public void createService(Service newService) {
		serviceList.add(newService);
		//adds the Service object "newService" to the "serviceList" ArrayList
	}
	public void removeService(int number) {
		serviceList.remove(number);
		//removes the service at the specified index "number"
	}
	public void removeService(Service oldService) {
		int index = serviceList.indexOf(oldService);
		serviceList.remove(index);
		//removes the specified "oldService" object from the "serviceList" ArrayList
	}
	public int getServiceListSize() {
		return serviceList.size();
		//returns the number of services stored in the "serviceList" ArrayList
	}
	public void write() {
		File dir = new File("services");
		if(!dir.isDirectory())
			dir.mkdirs();
		else
			for(File f : dir.listFiles())
				f.delete();
		for(Service a : serviceList) {
			try {
				PrintStream s =  new PrintStream("services\\"+a.getName()+".txt");
				s.println(a.getName());
				s.println(a.getCode());
				s.println(a.getProviderID());
				s.println(a.getFee());
				s.println(a.getDescription());
				s.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			}

		}
		//Prints the services stored in the "serviceList" ArrayList along with each service's information
	}

	public void deleteServices(int providerID) {
		for(Service s: serviceList) {
			if(s.getProviderID() == providerID)
				serviceList.remove(s);
		}
		//removes a service from serviceList based on the providerID passed in
	}
}
