package cs200spring2018team3;
/**
 * The Terminal Operator class. This allows the operator to add/delete/update members and providers.
 * @author jt1du
 * Code written by: Daniel Michaels
 */
public class TerminalOperator {

	private final ChocAnDatabase database;
	/**
	 * Sets the database to the ChocAn database d.
	 * @param d - ChocAn database
	 */
	public TerminalOperator(ChocAnDatabase d) {
		database = d;
	}
	/**
	 * Add a member account to the database.
	 * @param name - First and Last name
	 * @param address - Address from the address class
	 * @param email - Email address
	 * @return - Returns the account after being added
	 */
	public boolean addMember(String name, Address address, String email) {
		MemberAccount memAccount = new MemberAccount();

		memAccount.setAccountName(name);
		memAccount.setAccountAddress(address);
		memAccount.setAccountEmail(email);
		database.createAccount(memAccount);

		return database.findAccount(name);
	}
	/**
	 * Add a provider to the database.
	 * @param name - First and Last name
	 * @param address - Address from the address class
	 * @param email - Email address
	 * @return - Returns the account after being added.
	 */
	public boolean addProvider(String name, Address address, String email) {
		ProviderAccount proAccount = new ProviderAccount();

		proAccount.setAccountName(name);
		proAccount.setAccountAddress(address);
		proAccount.setAccountEmail(email);
		database.createAccount(proAccount);

		return database.findAccount(name);
	}
	/**
	 * Delete a member by ID from the database.
	 * @param idNumber - Member 9-digit ID
	 * @return - If the ID is found the member is deleted and returns true.
	 */
	public boolean deleteMember(int idNumber) {
		if (database.findAccount(idNumber))
		{
			database.deleteAccount(idNumber);
			return true;
		}

		return false;

	}
	/**
	 * Delete a member by name from the database.
	 * @param name - First and Last name
	 * @return - If the name is found the member is deleted and returns true.
	 */
	public boolean deleteMember(String name) {
		if (database.findAccount(name))
		{
			database.deleteAccount(name);
			return true;
		}

		return false;
	}
	/**
	 * Delete a provider by ID from the database.
	 * @param idNumber - Provider 9-digit ID
	 * @return - If the ID is found the Provider is deleted and returns true.
	 */
	public boolean deleteProvider(int idNumber) {
		if (database.findAccount(idNumber))
		{
			database.deleteAccount(idNumber);
			return true;
		}

		return false;
	}
	/**
	 * Delete a Provider by name from the database.
	 * @param name - First and Last name
	 * @return - If the name is found the Provider is deleted and returns true.
	 */
	public boolean deleteProvider(String name) {
		if (database.findAccount(name))
		{
			database.deleteAccount(name);
			return true;
		}

		return false;
	}
	/**
	 * Update member information in the database
	 * @param idNumber - Member 9-digit ID
	 * @param name - First and Last name
	 * @param address - Address from the address class
	 * @param email - Email address
	 * @return - If ID is found information is updated and returns true.
	 */
	public boolean updateMember(int idNumber, String name, Address address, String email) {
		if(database.findAccount(idNumber))
		{
			Account memAccount = database.getAccount(idNumber);
			memAccount.setAccountName(name);
			memAccount.setAccountAddress(address);
			memAccount.setAccountEmail(email);

			return true;
		}

		return false;

	}
	/**
	 * Update provider information in the database
	 * @param idNumber - Provider 9-digit ID
	 * @param name - First and Last name
	 * @param address - Address from the address class
	 * @param email - Email address
	 * @return - If ID is found information is updated and returns true.
	 */
	public boolean updateProvider(int idNumber, String name, Address address, String email) {
		if(database.findAccount(idNumber))
		{
			Account proAccount = database.getAccount(idNumber);
			proAccount.setAccountName(name);
			proAccount.setAccountAddress(address);
			proAccount.setAccountEmail(email);

			return true;
		}

		return false;
	}

	public boolean addMember(String name, String street, String city, String state, String zip, String email) {
		int newZip = Integer.parseInt(zip);
		Address address = new Address(street,city,state,newZip);
		return addMember(name,address,email);

	}

	public boolean addProvider(String name, String street, String city, String state, String zip, String email) {
		int newZip = Integer.parseInt(zip);
		Address address = new Address(street,city,state,newZip);
		return addProvider(name,address,email);
	}

	public boolean updateProvider(int idNumber, String name, String street, String city, String state, String zip,
			String email) {
		int newZip = Integer.parseInt(zip);
		Address address = new Address(street,city,state,newZip);
		return updateProvider(idNumber,name,address,email);
	}

	public boolean updateMember(int idNumber, String name, String street, String city, String state, String zip,
			String email) {
		int newZip = Integer.parseInt(zip);
		Address address = new Address(street,city,state,newZip);
		return updateMember(idNumber,name,address,email);
	}
}
