package cs200spring2018team3;

/**
 * Description:
 * @author
 *
 */

import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class TerminalManagerTimer {
	private ChocAnDatabase database;

	public TerminalManagerTimer() {
		super();
		this.database = null;
	}

	public TerminalManagerTimer(ChocAnDatabase database) {
		super();
		this.database = database;
	}

	public void createMemberReport(String name) {
		MemberAccount account = (MemberAccount)database.getAccount(name);
		ArrayList<ServiceRecord> records = new ArrayList<>();
		ServiceRecordController list = database.getServiceRecordList();
		for(int i= 0; i < list.getServiceRecordListSize(); i++) {
			ServiceRecord current = list.getServiceRecord(i);
			if(current.getMember().equals(account))
				records.add(current);
		}
		MemberReport memberReport = new MemberReport(account,list);
		memberReport.printReport();
	}

	public void createProviderReport(String name) {

		ProviderAccount account = (ProviderAccount) database.getAccount(name);
		ArrayList<ServiceRecord> records = new ArrayList<>();
		ServiceRecordController list = database.getServiceRecordList();
		for(int i= 0; i < list.getServiceRecordListSize(); i++) {
			ServiceRecord current = list.getServiceRecord(i);
			if(current.getMember().equals(account))
				records.add(current);
		}
		ProviderReport providerReport = new ProviderReport(account,list);

		providerReport.printReport();
	}

	public void createSummaryReport() {
		SummaryReport summaryReport = new SummaryReport(database.getServiceRecordList());
		summaryReport.printReport();
	}

	public void createEFTFile(Account account) {

		ServiceRecordController serviceRecordList = database.getServiceRecordList();
		ServiceRecord service;
		double total = 0;
		// Crawl through ServiceRecordList and if the provider name matches add the fee for the service to total
		for (int i=0; i<serviceRecordList.getServiceRecordListSize(); ++i) {
			service = serviceRecordList.getServiceRecord(i);
			if(service.getProvider().getAccountName() == account.getAccountName()) {
				total =+ service.getService().getFee();
			}
		}
	  // Write the name, number and total fee to be paid to a file
	  PrintWriter writer = null;
		try {
			writer = new PrintWriter(account.getAccountName() + "EFT.txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		writer.write(account.getAccountName());
		writer.write("\n");
		writer.write(account.getAccountNumber());
		writer.write("\n");
		writer.write((int)total);
	}

	public void produceReports() {

		ServiceRecord service;

		ServiceRecordController serviceRecordList = database.getServiceRecordList();
		ArrayList<MemberAccount> memberAccounts = new ArrayList<>();
		ArrayList<ProviderAccount> providerAccounts = new ArrayList<>();
		// Crawl through the Service Record List and add unique members to memberAccount list
		for (int i=0; i<serviceRecordList.getServiceRecordListSize();i++) {
			service = serviceRecordList.getServiceRecord(i);
			for (int j=0; j<memberAccounts.size(); ++j){
				if (service.getMember() == memberAccounts.get(i)) break;
				if (j == memberAccounts.size()-1) memberAccounts.add(service.getMember());
			}
		}
		// Crawl through the Service Record List and add unique providers to providerAccount list
		for (int i=0; i<serviceRecordList.getServiceRecordListSize();i++) {
			service = serviceRecordList.getServiceRecord(i);
			for (int j=0; j<providerAccounts.size(); ++j){
				if (service.getProvider() == providerAccounts.get(i)) break;
				if (j == providerAccounts.size()-1) providerAccounts.add(service.getProvider());
			}
		}

		Account account = null;
		// Send every member through to the print member report function
		for (int i=0; i<providerAccounts.size(); ++i) {
			account = memberAccounts.get(i);
			createMemberReport(account.getAccountName());
		}
		// Send every provider through to the print member report function
		for (int i=0; i<providerAccounts.size(); ++i) {
			account = providerAccounts.get(i);
			createProviderReport(account.getAccountName());
		}
		// Send every provider through to the EFT Function
		for (int i=0; i<providerAccounts.size(); ++i) {
			createEFTFile(providerAccounts.get(i));
		}
		// Call summary Report Function
		createSummaryReport();
	}
}