package cs200spring2018team3;

/**
 * Description: 
 * @author 
 * 
 */

public class Service {
	private double fee;
	private int code;
	private String description;
	private int providerID;
	private String name;
	

	public Service() {
		super();
		fee = 0.0;
		code = 0;
		description = "";
		providerID = 0;
		name = "";
	}
	
	public Service(double fee, int code, String description, int providerID, String name) {
		super();
		this.fee = fee;
		this.code = code;
		this.description = description;
		this.providerID = providerID;
		this.name = name;
	}

	public double getFee() {
		return fee;
	}
	
	public void setFee(double d) {
		this.fee = d;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getProviderID() {
		return providerID;
	}
	
	public void setProviderID(int providerID) {
		this.providerID = providerID;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
