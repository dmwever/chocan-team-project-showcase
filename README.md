# This is my showcase of our CS 200 ChocAn Team Project.
# Fellow coders: Daniel Michaels, Jason Logerquist, Nathan Hinton, Jarrod Duggan

# This project was assigned to develop and demonstrate teamwork capabilities between different CS majors, and to develop an understanding of the Software Engineering process.
# ChocAn is a fictional company that provides chocolate related services to customers through the assistance of service providers. We have developed a prototype for the central terminal of ChocAn.
# The central terminal provides access to any service within the ChocAn Database, and it stores information on any clients who are currently using ChocAn. It also sends bills and service records out to the appropriate people.